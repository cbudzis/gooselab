function Gooselab(pathname, frame_acc)

%Gooselab(pathname, frame_acc)
%
% Gooselab analyzes a video of human skin for the intensity of goosebumps. Type Gooselab to start the GUI.
% You can also use the command line functionality and analyze all videos located in one directory
% For each video you receive feedback on the goose-amp mean and maximum
%
% pathname   analyze alle files in this directory (absolute dir)
% frame_acc  accuracy, video is analyzed in steps of frame_acc frames.
%            for frame_acc = 1 all frames are analyzed (default)


clear global goose
close all
clc;

goosedir = fileparts(which('Gooselab.m'));
addpath(genpath(goosedir)); %add all subdirectories of Gooselab to Matlab path
%savepath;

global goose
goose.current = struct('batchmode',0,'isanalyzing',0,'isplaying',0,...
    'istoggling',0,'isrecording',0,'iFrame',1,'jFrame',1,...
    'nFramesDone',0,'spect_limy',0,'legend',[],'imgLenMax',0,'fft2Max',.1);
%Settings: analysis, process, visual
goose.set = struct(...
    'analysis',struct(...
        'gausswinsize',.5,...
        'detrend_errorlim',4,... %error tolerance limit before new detrend image updated
        'detrendfact',30,... %smoothing factor: high vals = lower detrend and slower! Choosing lower vals may remove gb frequency
        'spectbyf',2,... %correct for 2D pink noise by * f^2; fixed
        'fac',[1.5,1],...
        'basepolydegree',2,...
        'convwinsize',2,...
        'goosepix',12,...
        'gooserange',[10 14],...
        'specttype',1,...
        'basetype',3,...
        'spectpos',3),...
    'process',struct(...
        'framerange',[1 0],...
        'progressionmode',2,...
        'overwrite',0),...
    'visual',struct(...
        'imgLen',0,...
        'rgb_alpha',[1 1 1],...
        'fft2_lim',60,...
        'fft2_radalpha',2,...
        'spect_limx',.5,...
        'spect_limy',[],...
        'updategraphics',[1 1 1 1],...
        'rotate',0)); %-> goose.set.analysis ?!?
    goose.set.analysis.spectposL = {'goosepix','peak location','mean peak loc (all)','mean peak loc (is goose)','mean peak loc (no goose)'};
    goose.set.process.progressionmodeL = {'linear','progressive'};
%goose.set.analysis.detrend_errorlim = 4;  %error tolerance limit before new detrend image updated

goose.set.greenLED_thresh = .6;
goose.set.redLED_thresh = 1;
goose.set.markerNameL = {'LED Onset','LED-Offset','Goose-Onset','Goose-Offset'};

%Settings: visual
goose.set.visual.showspecL = {'radial spectogram','current baseline fit','amplitude x0 indicator','spectogram for gb-frames','spectogram for no gb-frames','mean baseline fit','legend'};
goose.set.visual.showspec = [1 0 1 0 0 0 0];
%Analysis: options
goose.video.nFrames = 0;
goose.video.vidobj = [];
%%
goose.gui.line_marker = [];
goose.gui.text_marker = [];

goose.version = struct('number',1.27, 'datestr','2018-07-05');
goose.version.videoext = {'.mkv','.avi','.mov','.mj2','.ogg','.ogv','.mp4','.mpg','.mpeg', '.m4v'};



if nargin == 0
    set(0,'DefaultUicontrolUnits','normalized');
    set(0,'DefaultTextFontUnits','normalized','DefaultTextFontSize',.5);
    set(0,'DefaultUicontrolFontUnits','normalized','DefaultUicontrolFontSize',.5);
    set(0,'DefaultAxesFontUnits','normalized','DefaultAxesFontSize',.5);

    %% build GUI
    goose.gui.fig_main = figure('Units','normalized','Position',[0 .03 1 .92],'Menubar','None',...
        'Name',['GooseLab ',sprintf('%3.2f',goose.version.number)],'Numbertitle','Off','KeyPressFcn','g_figkeypressfcn');

% Here we define the menus on top. The format is 
% { 'Menutitle', {'Title 1','callbackfn',seperator;
% 'Title 2','fn2',seperator};
% 'Menu 2 Title', { etc...
    menu = {
        'File', {
            'Load Video','g_open(1)',0;
            'Load Image','g_open(2)',0};
        'Settings', {
            'Analysis Settings','g_set_analysis',0;
            'Visual Settings','g_set_visual',0;
            'Refresh Graphics','g_set_refresh',0};
        'Analysis', {
            'Analyze','g_analyze_set',0;
            'Analyze current frame','g_analyze(1)',0;
            'Normalize','g_renorm_gui',1;
            'Reset analysis','g_reset(0)',1};
        'Tools', {
            'Find LED Marker','g_getmarker(0)',0;
            'Add Marker','g_modifymarker(1)',0;
            'Delete Marker','g_modifymarker(2)',0;
            'Remove LED Artifacts','remove_LEDartifact',0;
            'Spectrum','g_spectrum',1};
        'Play', {
            'Play video & sound','g_play(1)',0;
            'Toggle frames','toggle_frames',1;
            'File Mode','g_imaq',1;
            'Stream Mode','g_imaq',0};
        'Export', {
            'Single Picture','g_export(1)',0;
            'Animated Gif','g_export(2)',0;
            'Goose Values','g_export(3)',1};

        'Help', {
            'Info','g_info',0;
            'About Gooselab','g_logo',0}
    };
    sep = {'off','on'};
    for i=1:length(menu)
        submenu = uimenu(goose.gui.fig_main,'Label',menu{i,1});
        for j=1:length(menu{i,2}(:,1))
            entry = menu{i,2}(j,:);
            uimenu(submenu, 'Label', entry{1}, 'Callback', entry{2}, 'Separator', sep{entry{3}+1});
        end
    end

    gui_layout.panel_uppertimeline = uipanel('Title','Soundtrack','TitlePosition','centertop','FontWeight','bold','BorderType','none','BorderWidth',0,'Position',[0 0.8 1 0.2]);
    gui_layout.panel_lowertimeline = uipanel('Title','Time Course of Goosebump Values','TitlePosition','centertop','FontWeight','bold','BorderType','none','BorderWidth',0,'Position',[0 0.6 1 0.2]);
    gui_layout.panel_buttons  = uipanel('BorderType','none','BorderWidth',0,'Position',[0 0.5 1 0.1]);
    gui_layout.lower_left   = uipanel('Title','Frame of Skin Video','TitlePosition','centertop','FontWeight','bold','BorderType','none','BorderWidth',0,'Position',[0.0 0.00 0.4 0.5]);
    gui_layout.lower_center = uipanel('BorderType','none','BorderWidth',0,'Position',[0.4 0.00 0.2 0.5]);
    gui_layout.lower_right  = uipanel('BorderType','none','BorderWidth',0,'Position',[0.6 0.00 0.4 0.5]);
     
    goose.gui.ax_sound = axes('Parent',gui_layout.panel_uppertimeline,'OuterPosition',[0 .2 1 .8],'YLim',[-1, 1],'ButtonDownFcn','g_click','FontSize',.13);
    xlabel(goose.gui.ax_sound, 's');
    goose.gui.text_sound = uicontrol('Parent',gui_layout.panel_uppertimeline,'Style','text','Position',[0.05 0 .95 .2],'HorizontalAlignment','left');
    
    goose.gui.ax_gamp = axes('Parent',gui_layout.panel_lowertimeline,'OuterPosition',[0 .2 1 .8],'ButtonDownFcn','g_click','YLim',[0, .5],'FontSize',.13);
    xlabel(goose.gui.ax_gamp,'s');
     
    goose.gui.ax_video = axes('Parent',gui_layout.lower_left,'OuterPosition',[0 .2 1 .8],'FontSize',.04);
    set(goose.gui.ax_video, 'XTick', [], 'YTick', [], 'Box', 'on','XColor',[0 0 0], 'YColor', [0 0 0],'YDir','reverse');
    goose.gui.text_video = uicontrol('Parent',gui_layout.lower_left,'Style','text','Position',[0.05 .05 .95 .1],'HorizontalAlignment','left','FontSize',0.5);
 
    % Load Logo Image
    goose.current.pic = imagesc(imread('Projektlogo.jpg'),'Parent',goose.gui.ax_video,'ButtonDownFcn','g_open(1)');
    set(get(goose.current.pic,'Parent'), 'XTick', [], 'YTick', [], 'Box', 'on','XColor',[0 0 0], 'YColor', [0 0 0],'YDir','reverse');
    
    goose.gui.ax_gray = axes('Parent',gui_layout.lower_center,'OuterPosition',[0 0.6 1 0.4],'FontSize',.1);
    xlabel(goose.gui.ax_gray,'pix');
    ylabel(goose.gui.ax_gray,'pix');
    goose.gui.txt_gray_title = title(goose.gui.ax_gray,'Gray image');
     
    goose.gui.ax_fft2 = axes('Parent',gui_layout.lower_center,'OuterPosition',[0 0.2 1 0.4],'FontSize',.1);
    xlabel(goose.gui.ax_fft2,'inv.pix');
    ylabel(goose.gui.ax_fft2,'inv.pix');
    goose.gui.text_fft2_title = title(goose.gui.ax_fft2,'Fourier Transform');
    goose.gui.text_fft2Info = uicontrol('Parent',gui_layout.lower_center,'Style','text','Position',[0 0.05 1 .1],'HorizontalAlignment','center','FontSize',0.5);
     
    goose.gui.ax_spec = axes('Parent',gui_layout.lower_right,'OuterPosition',[0 0.2 1 0.8],'FontSize',.04);
    xlabel(goose.gui.ax_spec,'inv.pix');
    ylabel(goose.gui.ax_spec,'inv.pix');
    goose.gui.text_spec_title = title(goose.gui.ax_spec,'Radial Integral of Fourier Transform');

    % Buttons etc.
    goose.gui.butt_rec           = uicontrol('Parent',gui_layout.panel_buttons,'Position',[.62 .35 .05 .35],'String','record','Callback','g_record','FontSize',.5,'Enable','off');
    goose.gui.butt_play          = uicontrol('Parent',gui_layout.panel_buttons,'Position',[.70 .35 .05 .35],'String','play','Callback','g_play','FontSize',.5);
    goose.gui.chbx_analyze_while_playing = uicontrol('Parent',gui_layout.panel_buttons,'Position',[.83 .35 .13 .35],'Style','checkbox','String','on-the-fly analysis','Value',1,'FontSize',.6);

    goose.gui.edit_pos_sec       = uicontrol(uipanel('Parent',gui_layout.panel_buttons,'Title','Time','Position',[.1 .33 .05 .66]),'Style','edit','Position',[0 0 1 .9],'String','','FontSize',.55,'Enable','inactive');
    goose.gui.edit_pos_frame     = uicontrol(uipanel('Parent',gui_layout.panel_buttons,'Title','Frame','Position',[.18 .33 .05 .66]),'Style','edit','Position',[0 0 1 .9],'String','','FontSize',.55,'Callback','goto_frame');
    goose.gui.edit_gamp          = uicontrol(uipanel('Parent',gui_layout.panel_buttons,'Title','Goose-Amp','Position',[.29 .33 .07 .66]),'Style','edit','Position',[0 0 1 .9],'FontSize',.55,'Enable','inactive');
    goose.gui.edit_gamp_done     = uicontrol(uipanel('Parent',gui_layout.panel_buttons,'Title','Analyzed','Position',[.39 .33 .07 .66]),'Style','edit','Position',[0 0 1 .9],'FontSize',.4,'Enable','inactive');
    goose.gui.butt_stop_analysis = uicontrol('Parent',gui_layout.panel_buttons,'Position',[.39 0 .07 .35],'String','stop analysis','Callback','g_analyze(0)','Visible','off','FontSize',.5);
 	
 
    % Additional Load Button
    % text(50,365,'Load video...','Color',[1 1 1],'FontSize',0.5,'ButtonDownFcn','g_open(1)');

    drawnow;
    set(get(handle(gcf),'JavaFrame'), 'Maximized',true); % Maximize the window, should work with Matlab >R2008a, tested with Matlab <R2012b
    
else %batch mode

    if nargin < 2
        frame_acc = 1;
    end

    g_batchanalysis(pathname, frame_acc);    
    
end
